# Residential exemptions

This repository contains reports on the processing and automatic renewal of residential exemptions. In August of 2019, the Illinois Legislature [authorized the automatic renewal](https://www.ilga.gov/legislation/publicacts/fulltext.asp?Name=101-0453) of Senior Homestead Exemptions. See [our website](https://www.cookcountyassessor.com/senior-citizen-exemption) for more information. In 2024, our office plans to advocate for an extension of this law. In order to support that advocacy, we will provide a detailed report on the implementation of the law.

## 2019 renewal

In 2019, our office sent names and birthdays of seniors receiving the senior homestead exemption to LexisNexis and the Illinois Department of Public Health. LexisNexis can match names and birthdays against the Social Security Master death list, as well as other sources. Both vendors returned matched data to our agency. Both used exact match as the criteria for matching. The Data Science department wrote [custom code](https://gitlab.com/ccao-data-science---modeling/reports/residential-exemptions/-/blob/master/code/VW_HB833.sql) to produce lists of renewal and non-renewal PINs. 

Properties that were automatically renewed received a notice to that effect in the post. Properties that were not automatically renewed received a notice to that effect and an application form for the senior homestead exemption.

## 2020 renewal

The CCAO contracted with [The Exemption Project](https://www.exemptionproject.com/) to configure and deploy a customized instance of TrueRoll. This data platform gathers and analyses dozens of different data streams from public, private, and government sources. Using sophistaced artificial intelligence, the platform predicts the probability that each property is eligible for each residential exemption, and creates a rank-ordered list of properties to be examined by CCAO staff. By working that list, CCAO staff can ensure that every exemption is applied to all homes eligible for the exemption, and homes that are ineligible are not revceiving exemptions.
